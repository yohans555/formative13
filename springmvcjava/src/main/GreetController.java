

import java.time.LocalDate;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
  
@Controller
public class GreetController {
	LocalDate date = LocalDate.now();
  
    @RequestMapping("/greet")
    public ModelAndView showview() {
        ModelAndView mv = new ModelAndView();
        mv.setViewName("result.jsp");
        mv.addObject("result", "Hallo " + date);
        return mv;
    }
}
