
import java.util.ArrayList;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class PersonController {
	
	@RequestMapping("/person")
    public ModelAndView showview() {
		ArrayList<Person> list = new ArrayList<>();
		list.add(new Person("a","b","c@gmail"));
		list.add(new Person("c","d","1@gmail"));
		list.add(new Person("e","f","2@gmail"));
		list.add(new Person("g","h","3@gmail"));
		list.add(new Person("neko","mata","oka@gmail"));
		list.add(new Person("inu","gami","koro@gmail"));
		list.add(new Person("toki","sora","bolo@gmail"));
		list.add(new Person("char","lot","duno@gmail"));
		
        ModelAndView mv = new ModelAndView();
        mv.setViewName("result.jsp");
        mv.addObject("list", list);
        return mv;
    }
}
