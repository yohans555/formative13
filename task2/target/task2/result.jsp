<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>

	<head>
	<meta charset="ISO-8859-1">
	<title>List</title>
	</head>

	<body>
	<h3>List Persons: </h3>
	<c:forEach items="${list}" var="item">
		<li>
			<ul>${item.getName()}</ul>
			<ul>${item.getLastname()}</ul>
			<ul>${item.getEmail()}</ul>
		</li>
	</c:forEach>
	</body>
</html>